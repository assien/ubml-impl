/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.devcommon;

import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.configuration.WorkspaceHelper;

public class ManagerUtils {
    public static final String SPRING_SHELL_FLAG_NAME = "spring-shell";
    public static final String SPRING_SHELL_FLAG_TRUE = "1";
    public static final boolean SHELL_FLAG = SPRING_SHELL_FLAG_TRUE.equals(System.getProperty(SPRING_SHELL_FLAG_NAME));
    private static final FileServiceImp FILE_SERVICE = new FileServiceImp();
    /**
     * 获取开发根路径
     */
    private static String devRootPath;
    private static String mavenStoragePath;
    private static String metadataPackageLocation;

    public static String getDevRootPath() {
        if (SHELL_FLAG) {
            return devRootPath;
        }
        return WorkspaceHelper.getInstance().getDevRootPath();
    }

    public static void setDevRootPath(String path) {
        if (SHELL_FLAG) {
            devRootPath = path;
        }

    }

    public static String getMetadataPackageLocation() {
        if (SHELL_FLAG) {
            return metadataPackageLocation;
        }
        return FILE_SERVICE.getCombinePath(getDevRootPath(), "packages");
    }

    public static void setMetadataPackageLocation(String metadataPackageLoc) {
        if (SHELL_FLAG) {
            metadataPackageLocation = metadataPackageLoc;
        }
    }

    /**
     * @param path 获取绝对路径
     * @return
     */
    public static String getAbsolutePath(String path) {
        path = handlePath(path);
        if (SHELL_FLAG) {
            if (isAbsolutePath(path)) {
                return path;
            } else {
                throw new RuntimeException("当前路径不是绝对路径，请确认。当前路径为：" + path);
            }

        }
        String devRootPath = handlePath(getDevRootPath());
        if (path.startsWith(devRootPath)) {
            if (isAbsolutePath(path)) {
                return path;
            } else {
                throw new RuntimeException("当前路径不是绝对路径，请确认。当前路径为：" + path);
            }
        } else if (path.startsWith("/") || path.startsWith("\\")) {
            path = path.substring(1);
        }
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        String absolutePath = FILE_SERVICE.getCombinePath(devRootPath, path);
        // TODO 判断路径是否存在
        return handlePath(absolutePath);
    }

    public static boolean isAbsolutePath(String path) {
        return handlePath(path).startsWith("/") || path.contains(":/");
    }

    public static String handlePath(String path) {
        return path.replace("\\", "/");
    }

    public static String getMavenStoragePath() {
        if (SHELL_FLAG) {
            return mavenStoragePath;
        }
        return FILE_SERVICE.getCombinePath(getDevRootPath(), "maven");
    }

    public static void setMavenStoragePath(String mavenStorageP) {
        if (SHELL_FLAG) {
            mavenStoragePath = mavenStorageP;
        }
    }

    public static String getRalativePath(String relativeFullPath) {
        String relativePath;
        if (!getDevRootPath().isEmpty() && FILE_SERVICE.startWith(relativeFullPath, getDevRootPath())) {
            relativePath = FILE_SERVICE.getRelPath(relativeFullPath, getDevRootPath());
        } else {
            relativePath = relativeFullPath;
        }
        return relativePath;
    }
}
