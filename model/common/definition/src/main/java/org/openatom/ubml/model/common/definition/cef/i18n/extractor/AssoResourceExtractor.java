/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.i18n.extractor;

import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.i18n.context.CefResourcePrefixInfo;
import org.openatom.ubml.model.common.definition.cef.i18n.context.ICefResourceExtractContext;

public abstract class AssoResourceExtractor extends AbstractResourceExtractor {
    private GspAssociation asso;

    protected AssoResourceExtractor(GspAssociation asso, ICefResourceExtractContext context,
        CefResourcePrefixInfo parentResourceInfo) {
        super(context, parentResourceInfo);
        this.asso = asso;
    }

    protected void extractItems() {
        if (asso.getRefElementCollection() != null && asso.getRefElementCollection().size() > 0) {
            for (IGspCommonField item : asso.getRefElementCollection()) {
                getAssoRefFieldResourceExtractor(getContext(), getCurrentResourcePrefixInfo(), item).extract();
            }
        }
    }

    protected CefResourcePrefixInfo buildCurrentPrefix() {
        if (getParentResourcePrefixInfo() == null) {
            throw new RuntimeException("多语项抽取_字段" + asso.getBelongElement().getName() + "无对应的多语前缀信息。");
        }
        return getParentResourcePrefixInfo();
    }

    /**
     * 赋值节点的国际化项前缀
     */
    protected final void setPrefixInfo() {
        asso.setI18nResourceInfoPrefix(getCurrentResourcePrefixInfo().getResourceKeyPrefix());
    }

    protected abstract AssoRefFieldResourceExtractor getAssoRefFieldResourceExtractor(
        ICefResourceExtractContext context, CefResourcePrefixInfo assoPrefixInfo, IGspCommonField field);

}