/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.core.config;

import com.inspur.edp.jittojava.context.GenerateService;
import com.inspur.edp.jittojava.context.service.CommonService;
import com.inspur.edp.jittojava.core.CommonServiceImp;
import com.inspur.edp.jittojava.core.GenerateServiceImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Classname ServiceAutoConfiguration
 * @Description TODO
 * @Date 2019/7/30 13:43
 * @Created by liu_bintr
 * @Version 1.0
 */
@Configuration
public class ServiceAutoConfiguration {

    @Bean
    public CommonService createCommonService() {
        return new CommonServiceImp();
    }

    @Bean
    public GenerateService createGenerateService() {
        return new GenerateServiceImp();
    }
}
