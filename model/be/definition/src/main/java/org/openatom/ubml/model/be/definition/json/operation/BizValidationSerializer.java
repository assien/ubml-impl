/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.openatom.ubml.model.be.definition.beenum.BETriggerTimePointType;
import org.openatom.ubml.model.be.definition.beenum.RequestNodeTriggerType;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.Validation;
import org.openatom.ubml.model.common.definition.cef.collection.ValElementCollection;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

/**
 * The Josn Serializer Of Validation
 *
 * @ClassName: BizValidationSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizValidationSerializer extends BizOperationSerializer<Validation> {
    @Override
    protected void writeExtendOperationBaseProperty(JsonGenerator writer, BizOperation op) {

    }

    @Override
    protected void writeExtendOperationSelfProperty(JsonGenerator writer, BizOperation op) {
        Validation validation = (Validation)op;
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.VALIDATION_TYPE, validation.getValidationType().getValue());
        int triggerTimePointType = 0;
        for (BETriggerTimePointType timePointType : validation.getTriggerTimePointType()) {
            triggerTimePointType += timePointType.getValue();
        }
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.TRIGGER_TIME_POINT_TYPE, triggerTimePointType);
//        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Order, validation.getOrder());
//        WritePrecedingIds(writer, validation);
//        WriteSucceedingIds(writer, validation);
        int requestNodeTriggerType = 0;
        for (RequestNodeTriggerType requestNodeTrigger : validation.getRequestNodeTriggerType()) {
            requestNodeTriggerType += requestNodeTrigger.getValue();
        }
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.REQUEST_NODE_TRIGGER_TYPE, requestNodeTriggerType);

        writeRequestElements(writer, validation);
        writeRequestChildElements(writer, validation);
        writeValidationTriggerPoints(writer, validation);
    }

    private void writeRequestElements(JsonGenerator writer, Validation dtm) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.REQUEST_ELEMENTS);
        writeValidationElementCollection(writer, dtm.getRequestElements());
    }

    private void writeValidationElementCollection(JsonGenerator writer, ValElementCollection childElementsIds) {
        //[
        SerializerUtils.WriteStartArray(writer);
        if (childElementsIds.size() > 0) {
            for (String item : childElementsIds) {
                SerializerUtils.writePropertyValue_String(writer, item);
            }
        }
        //]
        SerializerUtils.WriteEndArray(writer);
    }

    private void writeRequestChildElements(JsonGenerator writer, Validation dtm) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.REQUEST_CHILD_ELEMENTS);
        HashMap<String, ValElementCollection> dic = dtm.getRequestChildElements();
        SerializerUtils.WriteStartArray(writer);
        if (dic != null && dic.size() > 0) {
            for (Map.Entry<String, ValElementCollection> item : dic.entrySet()) {
                SerializerUtils.writeStartObject(writer);
                SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.REQUEST_CHILD_ELEMENT_KEY, item.getKey());
                SerializerUtils.writePropertyName(writer, BizEntityJsonConst.REQUEST_CHILD_ELEMENT_VALUE);
                writeValidationElementCollection(writer, item.getValue());
                SerializerUtils.writeEndObject(writer);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }

    private void writeValidationTriggerPoints(JsonGenerator writer, Validation val) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.VALIDATION_TRIGGER_POINTS);
        HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> dic = val.getValidationTriggerPoints();
        SerializerUtils.WriteStartArray(writer);
        if (dic != null && dic.size() > 0) {
            for (Map.Entry<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> item : dic.entrySet()) {
                SerializerUtils.writeStartObject(writer);
                SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.VALIDATION_TRIGGER_POINT_KEY, item.getKey().getValue());
                int requestNodeTriggerType = 0;
                for (RequestNodeTriggerType trigger : item.getValue()) {
                    requestNodeTriggerType += trigger.getValue();
                }
                SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.VALIDATION_TRIGGER_POINT_VALUE, requestNodeTriggerType);
                SerializerUtils.writeEndObject(writer);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }
}
