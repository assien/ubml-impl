/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package webapi.config;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import webapi.FileSystemWebApi;
import webapi.GspProjectServiceWebApi;
import webapi.MdServiceWebApi;
import webapi.MetadataConfigServiceWebApi;
import webapi.MetadataPackageServiceWebApi;
import webapi.MetadataProjectServiceWebApi;
import webapi.MetadataServiceWebApi;
import webapi.ProjectDeployServiceWebApi;
import webapi.ProjectExtendWebApi;
import webapi.RepoRefWebApi;

//todo
//@Configuration(proxyBeanMethods = false)
@Configuration()
public class WebApiConfiguration {
    @Bean
    public RESTEndpoint gspFSEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/file-system",
            new FileSystemWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspProjectEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/gsp-projects",
            new GspProjectServiceWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspMdEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/mdservice",
            new MdServiceWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspConfigEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/metadata-configs",
            new MetadataConfigServiceWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspMdPkgEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/metadata-packages",
            new MetadataPackageServiceWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspMdProjectEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/metadata-projects",
            new MetadataProjectServiceWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspMetadataEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/metadatas",
            new MetadataServiceWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspDeployEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/deploy-projects",
            new ProjectDeployServiceWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspExtendEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/extend-projects",
            new ProjectExtendWebApi()
        );
    }

    @Bean
    public RESTEndpoint gspRepoEndpoint() {
        return new RESTEndpoint(
            "/dev/main/v1.0/repo-packages",
            new RepoRefWebApi()
        );
    }
}
