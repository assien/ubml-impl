/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.sync;

import com.inspur.edp.lcm.metadata.core.manager.PomManager;
import java.io.File;
import java.io.IOException;
import org.apache.maven.model.Model;

public class InstallSingleJarRunnable extends InstallJarRunnable {

    @Override
    public void run() {
        for (File file : fileServiceImp.getAllFiles(downloadPath)) {
            if (file.getName().contains("SNAPSHOT") && fileServiceImp.getExtension(file).equals(".pom")) {
                PomManager pomManager = new PomManager();
                Model model = pomManager.getModel(file.getPath());
                if (model != null) {
                    String mvnCommand = String.format(" dependency:get -DgroupId=%s -DartifactId=%s -Dversion=%s -Dskip=true -Dtransitive=false -U", model.getGroupId(), model.getArtifactId(), model.getVersion().substring(1));
                    mavenUtilsCore.exeMavenCommand(pomDirPath, mavenPath, mvnCommand);
                }
            }
        }
        if (fileServiceImp.isDirectoryExist(downloadPath)) {
            try {
                fileServiceImp.deleteAllFilesUnderDirectory(downloadPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
