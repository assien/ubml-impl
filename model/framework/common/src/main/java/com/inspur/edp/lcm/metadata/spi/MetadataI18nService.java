/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.spi;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.I18nResource;
import java.util.List;

public interface MetadataI18nService {
    /// 合并（用于加载翻译后的元数据）
    /// <param name="metadata">基础元数据</param>
    /// <param name="resource">多语资源</param>
    GspMetadata merge(GspMetadata metadata, List<I18nResource> resource);

    /// 此方法在运行时 获取元数据时，用不到。
    /// 此方法用于在设计时抽取资源元数据内容。
    /// 从基础元数据抽取资源元数据内容
    /// <param name="metadata">基础元数据</param>
    I18nResource getResourceItem(GspMetadata metadata);
}
