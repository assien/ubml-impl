/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.file.system;

import com.inspur.edp.lcm.file.system.monitor.FileAltMonitorImpl;
import com.inspur.edp.lcm.metadata.api.entity.FileType;
import com.inspur.edp.lcm.metadata.api.service.FileAltMonitor;
import com.inspur.edp.lcm.metadata.api.service.FileSystemService;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.core.session.WebSession;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;

public class FileSystemServiceImp implements FileSystemService {
    FileServiceImp fileServiceImp = new FileServiceImp();

    @Override
    public void showInExplorer(String path) throws IOException {
        WebSession webSession = (WebSession) CAFContext.current.getCurrentSession();
        if (!webSession.getRemoteAddress().equals("127.0.0.1") && !webSession.getRemoteAddress().equals("0:0:0:0:0:0:0:1")) {
            throw new RuntimeException("不支持访问远程路径。");
        }
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        absolutePath = fileServiceImp.isFileExist(absolutePath) ? fileServiceImp.getDirectoryName(absolutePath) : absolutePath;
        if (!fileServiceImp.isDirectoryExist(absolutePath)) {
            throw new RuntimeException("路径不存在：" + path + "。");
        }

        File file = new File(absolutePath);
        try {
            System.setProperty("java.awt.headless", "false");
            Desktop.getDesktop().open(file);
        } catch (Exception e) {
            openDirectoryByShell(absolutePath);
        }
    }

    @Override
    public FileAltMonitor createFileMonitor(String path, FileType type, FileAlterationListenerAdaptor adaptor) {
        return new FileAltMonitorImpl(path, type, adaptor);
    }

    private void openDirectoryByShell(String path) throws IOException {
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            String[] cmd = new String[5];
            cmd[0] = "cmd";
            cmd[1] = "/c";
            cmd[2] = "start";
            cmd[3] = " ";
            cmd[4] = new File(path).getAbsolutePath();
            Runtime.getRuntime().exec(cmd);
        } else {
            throw new RuntimeException("仅支持windows系统");
        }
    }
}
