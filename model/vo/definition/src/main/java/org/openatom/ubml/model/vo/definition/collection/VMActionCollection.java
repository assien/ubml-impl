/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.collection;

import java.io.Serializable;
import org.openatom.ubml.model.common.definition.cef.collection.BaseList;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;

/**
 * The Collection Of View Model Action
 *
 * @ClassName: VMActionCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VMActionCollection extends BaseList<ViewModelAction> implements Cloneable, Serializable {

    /**
     * 根据ID获取操作
     *
     * @param id 操作ID
     * @return 操作
     */
    public final ViewModelAction getItem(String id) {
        for (ViewModelAction item : this) {
            if (item.getID().equals(id)) {
                return item;
            }
        }
        return null;
    }

    /**
     * 克隆
     *
     * @return VO上的操作集合
     */
    @Override
    public final VMActionCollection clone() {
        VMActionCollection collections = new VMActionCollection();
        for (ViewModelAction op : this) {
            ViewModelAction tempVar = op.clone();
            collections.add((ViewModelAction) tempVar);
        }

        return collections;
    }

    /**
     * 重载Equals方法
     *
     * @param obj 要比较的对象
     * @return 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        return equals((VMActionCollection) obj);
    }

    /**
     * 当前对象是否等于同一类型的另一个对象。
     *
     * @param other 与此对象进行比较的对象。
     * @return 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    protected boolean equals(VMActionCollection other) {
        if (this.size() != other.size()) {
            return false;
        }
        for (ViewModelAction item : this) {
            ViewModelAction otherItem = other.getItem(item.getID());
            if (otherItem == null) {
                return false;
            }
            if (!item.equals(otherItem)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean remove(Object action) {
        if (action == null) {
            return false;
        }
        String id = ((ViewModelAction) action).getID();
        if (id == null) {
            return false;
        }
        super.removeIf(item -> id.equals(item.getID())
        );
        return true;
    }

    public boolean removeById(String actionId) {

        if (actionId == null || "".equals(actionId)) {
            return false;
        }
        super.removeIf(item ->
            actionId.equals(item.getID())
        );
        return true;
    }

}
