/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.i18n.merger;

import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.i18n.context.ICefResourceMergeContext;

;

public abstract class AssoResourceMerger extends AbstractResourceMerger {
    private GspAssociation asso;

    protected AssoResourceMerger(GspAssociation asso,
        ICefResourceMergeContext context) {
        super(context);
        this.asso = asso;
    }

    @Override
    protected void mergeItems() {
        if (asso.getRefElementCollection() != null && asso.getRefElementCollection().size() > 0) {
            for (IGspCommonField item : asso.getRefElementCollection()) {
                getAssoRefFieldResourceMerger(getContext(), item).merge();
            }
        }
    }

    protected abstract AssoRefFieldResourceMerger getAssoRefFieldResourceMerger(
        ICefResourceMergeContext context, IGspCommonField field);

}
