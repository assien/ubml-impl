/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.manager;

public class MavenCommandGenerator {
    private String outputDirectory = "";
    private String includeTypes = "";
    private String excludeArtifactIds = "";
    private boolean copyPom;
    private boolean excludeTransitive;
    private boolean forceUpdate;

    public String generateCopydependencies() {
        StringBuilder stringBuilder = new StringBuilder("dependency:copy-dependencies");
        if (!outputDirectory.isEmpty()) {
            stringBuilder.append(" -DoutputDirectory=");
            stringBuilder.append(outputDirectory);
        }
        if (!includeTypes.isEmpty()) {
            stringBuilder.append(" -DincludeTypes=");
            stringBuilder.append(includeTypes);
        }
        if (!excludeArtifactIds.isEmpty()) {
            stringBuilder.append(" -DexcludeArtifactIds=");
            stringBuilder.append(excludeArtifactIds);
        }
        if (copyPom) {
            stringBuilder.append(" -Dmdep.copyPom");
        }
        if (excludeTransitive) {
            stringBuilder.append(" -DexcludeTransitive");
        }
        if (forceUpdate) {
            stringBuilder.append(" -U");
        }
        return stringBuilder.toString();
    }

    public String generateInstall() {
        StringBuilder stringBuilder = new StringBuilder("install");
        if (forceUpdate) {
            stringBuilder.append(" -U");
        }
        return stringBuilder.toString();
    }

    public void setOutputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public void setIncludeTypes(String includeTypes) {
        this.includeTypes = includeTypes;
    }

    public void setExcludeArtifactIds(String excludeArtifactIds) {
        this.excludeArtifactIds = excludeArtifactIds;
    }

    public void setCopyPom(boolean copyPom) {
        this.copyPom = copyPom;
    }

    public void setExcludeTransitive(boolean excludeTransitive) {
        this.excludeTransitive = excludeTransitive;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }
}
