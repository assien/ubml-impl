/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation;

import org.openatom.ubml.model.be.definition.GspBizEntityObject;
import org.openatom.ubml.model.be.definition.beenum.BEOperationType;
import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.common.definition.cef.util.Guid;
import org.springframework.util.ObjectUtils;

/**
 * The Definition Of Biz Operation
 *
 * @ClassName: BizOperation
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizOperation implements Cloneable {

    private String privateID;
    /**
     * ҵ������
     */
    private String privateCode;
    /**
     * ��ʾ����
     */
    private String privateName;
    /**
     * ����
     */
    private String privateDescription;
    /**
     * ��Ӧ����ʵ��Id
     */
    private String privateComponentId;
    /**
     * ��Ӧ����ʵ��Name
     */
    private String privateComponentName;
    /**
     * ��Ӧ����ʵ�����
     */
    private String privateComponentPkgName;
    /**
     * �Ƿ�ɼ�
     */
    private boolean privateIsVisible;
    /**
     * ������Node�ڵ�
     */
    private GspBizEntityObject privateOwner;
    /**
     * ������չģ��Ԫ����ID
     */
    private String privateBelongModelID;
    private boolean privateIsRef;
    private boolean privateIsGenerateComponent;

    // /// <summary>
    ///// ����Id
    ///// </summary>
    // public virtual string MethodId { get; set; }
    private CustomizationInfo customizationInfo = new CustomizationInfo();

    public final String getID() {
        return privateID;
    }

    public final void setID(String value) {
        privateID = value;
    }

    public final String getCode() {
        return privateCode;
    }

    public final void setCode(String value) {
        privateCode = value;
    }

    public final String getName() {
        return privateName;
    }

    public final void setName(String value) {
        privateName = value;
    }

    public final String getDescription() {
        return privateDescription;
    }

    public final void setDescription(String value) {
        privateDescription = value;
    }

    public String getComponentId() {
        return privateComponentId;
    }

    public void setComponentId(String value) {
        privateComponentId = value;
    }

    public String getComponentName() {
        return privateComponentName;
    }

    public void setComponentName(String value) {
        privateComponentName = value;
    }

    public String getComponentPkgName() {
        return privateComponentPkgName;
    }

    public void setComponentPkgName(String value) {
        privateComponentPkgName = value;
    }

    public final boolean getIsVisible() {
        return privateIsVisible;
    }

    public final void setIsVisible(boolean value) {
        privateIsVisible = value;
    }

    /**
     * ��������
     */
    public abstract BEOperationType getOpType();

    public final GspBizEntityObject getOwner() {
        return privateOwner;
    }

    public final void setOwner(GspBizEntityObject value) {
        privateOwner = value;
    }

    public final String getBelongModelID() {
        return privateBelongModelID;
    }

    public final void setBelongModelID(String value) {
        privateBelongModelID = value;
    }

    public final boolean getIsRef() {
        return privateIsRef;
    }

    public final void setIsRef(boolean value) {
        privateIsRef = value;
    }

    public final boolean getIsGenerateComponent() {
        return privateIsGenerateComponent;
    }

    public final void setIsGenerateComponent(boolean value) {
        privateIsGenerateComponent = value;
    }

    public CustomizationInfo getCustomizationInfo() {
        return this.customizationInfo;
    }

    public void setCustomizationInfo(CustomizationInfo customizationInfo) {
        this.customizationInfo = customizationInfo;
    }



    // ����

    /**
     * Equals����
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return equals((BizOperation)obj);
    }

    /**
     * Equals����
     *
     * @param other
     * @return
     */
    protected boolean equals(BizOperation other) {
        // && Equals(MethodId, other.MethodId)
        // && IsEnabled == other.IsEnabled
        // && IsExcuteAfterVD.equals(other.IsExcuteAfterVD)
        // && IsOnlyExcuteAfterVD.equals(other.IsOnlyExcuteAfterVD)
        // && IsFinal.equals(other.IsFinal)

        //getComponentId().equals(other.getComponentId())
        return getID().equals(other.getID()) && getCode().equals(other.getCode()) && getName().equals(other.getName())
                && ObjectUtils.nullSafeEquals(getComponentId(), other.getComponentId()) && (new Boolean(getIsVisible())).equals(other.getIsVisible())
                && getOpType() == other.getOpType() && (new Boolean(getIsRef())).equals(other.getIsRef());
    }

    /**
     * ��дGetHashCode
     *
     * @return
     */
    @Override
    public int hashCode() {
        {
            int hashCode = (getID() != null ? getID().hashCode() : 0);
            hashCode = (hashCode * 397) ^ (getCode() != null ? getCode().hashCode() : 0);
            hashCode = (hashCode * 397) ^ (getName() != null ? getName().hashCode() : 0);
            // hashCode = (hashCode * 397) ^ (ElementName != null ?
            // ElementName.getHashCode() : 0);
            // hashCode = (hashCode * 397) ^ (MethodId != null ? MethodId.getHashCode() :
            // 0);
            hashCode = (hashCode * 397) ^ (getComponentId() != null ? getComponentId().hashCode() : 0);
            hashCode = (hashCode * 397) ^ (new Boolean(getIsVisible())).hashCode();
            hashCode = (hashCode * 397) ^ (int)getOpType().getValue();
            hashCode = (hashCode * 397) ^ (new Boolean(getIsRef())).hashCode();
            // hashCode = (hashCode * 397) ^ (IsEnabled.getHashCode() * 397);
            // hashCode = (hashCode * 397) ^ (IsExcuteAfterVD.getHashCode() * 397);
            // hashCode = (hashCode * 397) ^ (IsOnlyExcuteAfterVD.getHashCode() * 397);
            // ^ IsFinal.getHashCode();
            return hashCode;
        }
    }

    /**
     * ��¡BE����
     *
     * @param isGenerateId �Ƿ�����Id
     * @return ����һ���µĶ�������
     */
    public BizOperation clone(boolean isGenerateId) {
        Object tempVar = null;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
        BizOperation result = (BizOperation)((tempVar instanceof BizOperation) ? tempVar : null);

        if (isGenerateId) {
            result.setID(Guid.newGuid().toString());
            result.setCode("Copyof" + result.getCode());
        }

        return result;
    }

    /**
     * ��¡BE����
     *
     * @return ���ص�ǰ�����ĸ���
     */
    public BizOperation clone() {
        return clone(false);
    }

    /**
     * ��дToString
     *
     * @return ���ض�������
     */
    @Override
    public String toString() {
        return String.format("Id:%1$s,Code:%2$s,Name:%3$s,ComponentId:%4$s", getID(), getCode(), getName(),
                getComponentId());
    }

    protected final void mergeOperationBaseInfo(BizOperation dependentOperation) {
        setCode(dependentOperation.getCode());
        setName(dependentOperation.getName());
        setDescription(dependentOperation.getDescription());
        // MethodId = dependentOperation.MethodId;
        // TODO:method��merge
        setComponentId(dependentOperation.getComponentId());
        setBelongModelID(dependentOperation.getBelongModelID());
        setIsVisible(dependentOperation.getIsVisible());
    }


}