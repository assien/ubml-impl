/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.cache;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.ProjectMetadataCache;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 元数据设计时缓存
 */
public class MetadataDevCacheManager {

    MetadataDevCacheManager() {
    }

    //某个工程中的元数据列表
    private static Map<String, List<GspMetadata>> metadataListInProject = new HashMap();
    //工程中元数据所在位置缓存
    private static Map<String, ProjectMetadataCache> projectMetadataCacheMap = new ConcurrentHashMap();

    // 用来存储元数据设计时服务上下文信息，放到分布式缓存，保证服务重启不丢失。
//    private String metadataContextServerNode = "MetadataContextServer";
    private static ReentrantLock lock = new ReentrantLock();

    public static List<GspMetadata> getMetadataListInProject(String key) {
        lock.lock();
        try {
            return metadataListInProject.get(key);
        } finally {
            lock.unlock();
        }
    }

    public static void removeMetadataListInProject(String key) {
        metadataListInProject.remove(key);
    }

    public static void setMetadataListInProjectCache(String projectPath, List<GspMetadata> metadataList) {
        lock.lock();
        try {
            metadataListInProject.put(projectPath, metadataList);
        } finally {
            lock.unlock();
        }
    }

    public static void clearMetadataListCache() {
        metadataListInProject.clear();
    }

    public static void removeProjectMetadataCacheMap(String key) {
        lock.lock();
        try {
            projectMetadataCacheMap.remove(key);
        } finally {
            lock.unlock();
        }
    }

    public static Object getProjectMetadataCacheMap(String key) {
        lock.lock();
        try {
            return projectMetadataCacheMap.get(key);
        } finally {
            lock.unlock();
        }
    }

    public static void setProjectMetadataCacheMap(String key, ProjectMetadataCache projectMetadataCache) {
        lock.lock();
        try {
            projectMetadataCacheMap.put(key, projectMetadataCache);
        } finally {
            lock.unlock();
        }
    }
}
