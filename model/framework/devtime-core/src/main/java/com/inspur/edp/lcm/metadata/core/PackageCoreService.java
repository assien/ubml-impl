/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core;

import com.inspur.edp.ide.setting.api.entity.MavenSettings;
import com.inspur.edp.ide.setting.api.entity.MavenSettingsMirror;
import com.inspur.edp.ide.setting.api.entity.MavenSettingsServer;
import com.inspur.edp.ide.setting.api.service.MavenService;
import com.inspur.edp.jittojava.context.GenerateService;
import com.inspur.edp.lcm.metadata.api.entity.ApiModuleInfo;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageHeader;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageVersion;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.lcm.metadata.api.entity.ProjectHeader;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenComponent;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenComponents;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenPackageRefs;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MetadataInfoInMaven;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MetadataRepo;
import com.inspur.edp.lcm.metadata.api.mvnEntity.PackageSourceDto;
import com.inspur.edp.lcm.metadata.api.mvnEntity.PackageWithMetadataInfo;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.MavenUtils;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.lcm.metadata.core.entity.MdprojInfoDto;
import com.inspur.edp.lcm.metadata.core.entity.PackageType;
import com.inspur.edp.lcm.metadata.core.exception.MvnException;
import com.inspur.edp.lcm.metadata.core.exception.MvnExceptionEnum;
import com.inspur.edp.lcm.metadata.core.manager.IndexServerManager;
import com.inspur.edp.lcm.metadata.core.manager.MavenCommandGenerator;
import com.inspur.edp.lcm.metadata.core.manager.MavenDeploymentForMdpkgManager;
import com.inspur.edp.lcm.metadata.core.manager.PackageNameManager;
import com.inspur.edp.lcm.metadata.core.manager.PomManager;
import com.inspur.edp.lcm.metadata.core.manager.VersionManager;
import com.inspur.edp.lcm.metadata.core.persistence.RepositoryFactory;
import com.inspur.edp.lcm.metadata.core.sync.CopyToPackagesRunnable;
import com.inspur.edp.lcm.metadata.core.sync.InstallJarRunnable;
import com.inspur.edp.lcm.metadata.core.sync.InstallSingleJarRunnable;
import com.inspur.lcm.metadata.logging.LoggerDisruptorQueue;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.DeploymentRepository;
import org.apache.maven.model.DistributionManagement;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhaoleitr
 */
public class PackageCoreService {
    private static final Logger log = LoggerFactory.getLogger(PackageCoreService.class);

    private FileServiceImp fileServiceImp = new FileServiceImp();
    private MavenUtilsCore mavenUtilsCore = new MavenUtilsCore();
    private MetadataProjectCoreService metadataProjectCoreService = new MetadataProjectCoreService();

    private String downloadPath;
    private final DecimalFormat df;
    private MetadataProject metadataProject;
    private final Lock lock = new ReentrantLock();
    private final RestTemplate restTemplate = new RestTemplate();
    private ApiModuleInfo apiModuleInfo = null;
    private List<String> downloadMdpkgPaths;
    private List<MavenSettingsMirror> publicMirrors = new ArrayList<>();
    private String alimavenFlag = "maven.aliyun.com";
    private Map<String, String> depModules;

    public PackageCoreService() {
        String path = System.getenv("Path");
        if (path != null) {
            String[] allPath = path.split(";");
            for (String s : allPath) {
                if (s.contains("maven")) {
                    s = s.substring(0, s.length() - 4);
                    System.setProperty("MAVEN_HOME", s);
                }
            }
        }
        df = new DecimalFormat("0.000");

        depModules = new HashMap<>();
        depModules.put("comp", "api");
        depModules.put("core", "comp");
        depModules.put("runtime", "core");
        MavenSettingsMirror gspMirror = new MavenSettingsMirror("gsp-releases-repo", "gsp-releases", "https://repos.iec.io/repository/maven-gsp-releases/", "gsp-rep");
        MavenSettingsMirror bfMirror = new MavenSettingsMirror("bf-releases-repo", "bf-releases", "https://repos.iec.io/repository/maven-bf-releases/", "bf-rep");
        publicMirrors.add(gspMirror);
        publicMirrors.add(bfMirror);

    }

    // 解析型工程编译comp构件
    public void compile(String absolutePath, String modules, String mavenPath) {
        if ("all".equals(modules) || "comp".equals(modules)) {
            // 编译前检查代码是否含有代码，以及代码是否变更
            boolean containsCodeFlag = metadataProjectCoreService.containsCode(absolutePath, "comp");
            if (!containsCodeFlag) {
                return;
            }
            boolean codeChangedFlag = metadataProjectCoreService.codeChangesDetected(absolutePath, "comp");
            if (!codeChangedFlag) {
                return;
            }
        }

        String javaProjPath = fileServiceImp.getCombinePath(absolutePath, "java" + File.separator + "code");
        final MavenUtilsCore mavenUtilsCore = new MavenUtilsCore();
        if (!mavenUtilsCore.exeMavenCommand(javaProjPath, mavenPath, handleCompileGoal(modules))) {
            final String errorMessage = mavenUtilsCore.getErrorMessage();
            throw new MvnException(MvnExceptionEnum.GSP_LCM_MVN_0001.toString(), errorMessage, new RuntimeException(errorMessage));
        }

        if ("all".equals(modules) || "comp".equals(modules)) {
            // 编译后更新代码变更记录
            afterCompile(absolutePath);
        }
    }

    public void batchOperation(String absolutePath, String mavenPath, List<String> operations) {
        List<String> projPaths = new ArrayList<>();
        metadataProjectCoreService.getProjPathsInPath(absolutePath, projPaths);
        Map<String, MdprojInfoDto> mdprojInfoDtos = metadataProjectCoreService.transformToMdprojInfoDto(projPaths);
        List<String> buildOrder = metadataProjectCoreService.getBuildOrder(mdprojInfoDtos);
        GenerateService generateService = SpringBeanUtils.getBean(GenerateService.class);
        List<String> errorMsgs = new ArrayList<>();
        for (String projPath : buildOrder) {
            try {
                String start = "开始操作：" + projPath + "-----------------";
                LoggerDisruptorQueue.publishEvent("[LCM Info]" + start, CAFContext.current.getUserId());
                if (operations.contains("generate")) {
                    generateService.generate(projPath);
                }
                if (operations.contains("compile")) {
                    compile(projPath, "api,core,comp", mavenPath);
                }
                String end = "完成操作：" + projPath + "-----------------";
                LoggerDisruptorQueue.publishEvent("[LCM Info]" + end, CAFContext.current.getUserId());
            } catch (Exception e) {
                String excep = "异常结束：" + projPath + "-----------------";
                LoggerDisruptorQueue.publishEvent("[LCM Error]" + excep, CAFContext.current.getUserId());
                StringBuilder sb = new StringBuilder();
                sb.append(projPath);
                sb.append(":");
                sb.append(e.getMessage());
                errorMsgs.add(sb.toString());
                e.printStackTrace();
            }
        }
        try {
            // 防止其他的日志输出比这个晚。
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!errorMsgs.isEmpty()) {
            String log = "[LCM Error]以下工程批量操作失败，请排除问题后重新编译：";
            LoggerDisruptorQueue.publishEvent(log, CAFContext.current.getUserId());
            for (String msg : errorMsgs) {
                LoggerDisruptorQueue.publishEvent("[LCM Error]" + msg, CAFContext.current.getUserId());
            }
        } else {
            LoggerDisruptorQueue.publishEvent("[LCM Success]批量操作完成。", CAFContext.current.getUserId());
        }
    }

    public void restore(String absolutePath, Boolean force, String mavenPath, String packagePath) {
        if (Utils.isNullOrEmpty(absolutePath)) {
            throw new IllegalArgumentException("工程路径不能为空");
        }
        MetadataProjectCoreService projectCoreService = new MetadataProjectCoreService();
        absolutePath = projectCoreService.getProjPath(absolutePath);
        MetadataProject metadataProjInfo = projectCoreService.getMetadataProjInfo(absolutePath);
        Utils.checkNPE(metadataProjInfo, "获取元数据工程信息失败，工程路径为：" + absolutePath);

        List<MavenPackageRefs> mavenPackageRefsList = metadataProjInfo.getMavenPackageRefs();
        if (Utils.isNullOrEmpty(mavenPackageRefsList)) {
            return;
        }

        // bo间工程引用
        List<ProjectHeader> projectHeaderList = metadataProjInfo.getProjectRefs();
        removeProjectRefs(absolutePath, mavenPackageRefsList, projectHeaderList);

        boolean installFlag;
        List<MavenPackageRefs> needInstallPackages = new ArrayList<>();
        for (MavenPackageRefs ref : mavenPackageRefsList) {
            installFlag = getInstallFlag(ref, force, mavenPath, packagePath);
            if (installFlag) {
                needInstallPackages.add(ref);
            }
        }
        try {
            log.info("元数据开始下载");
            doRestore(needInstallPackages, absolutePath, force, mavenPath, packagePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void addDependencyAndRestore(String absolutePath, List<String> metadataPackages, String mavenPath,
        String packagePath) {
        String projPath = metadataProjectCoreService.getProjPath(absolutePath);
        MetadataProject metadataProject = metadataProjectCoreService.getMetadataProjInfo(projPath);
        metadataPackages.forEach(item -> {
            MavenPackageRefs mavenPackageRefs = handleMavenPackageName(item);
            MetadataPackageHeader packageHeader = handleMetadataPackageName(item);
            //1、更新maven依赖
            metadataProjectCoreService.updateMavenRefs(projPath, metadataProject, mavenPackageRefs);
            //2、更新metadata依赖
            metadataProjectCoreService.updateRefs(projPath, metadataProject, packageHeader);
            //3、安装特定包
            restoreSpecificPackage(metadataProject.getProjectPath().substring(0, metadataProject.getProjectPath().indexOf(Utils.getMetadataProjPath()) - 1), mavenPackageRefs, mavenPath, packagePath);
        });
    }

    public void installMdpkg(String groupId, String artifactId, String version, String absolutePath, String mavenPath,
        String packagePath) throws IOException {
        try {
            installMdpkg(groupId, artifactId, version, absolutePath, false, mavenPath, packagePath);
        } catch (IOException e) {
            log.error("获取最新元数据包失败，请查看mdproj文件中引用的元数据包是否正确");
            throw new RuntimeException(e);
        }
    }

    public void doRestore(List<MavenPackageRefs> refs, String absolutePath, boolean forceUpdateFlag, String mavenPath,
        String packagePath) throws IOException {
        // 防止工作空间未初始化maven文件夹
        if (!fileServiceImp.isDirectoryExist(mavenPath)) {
            fileServiceImp.createDirectory(mavenPath);
        }

        String metadataPath = fileServiceImp.getCombinePath(absolutePath, Utils.getMetadataProjPath());
        String pomPath = fileServiceImp.getCombinePath(metadataPath, MavenUtils.POM_FILE);
        ProcessMode processMode = metadataProjectCoreService.getProcessMode(absolutePath);
        try {
            // 创建pom
            final PomManager pomManager = new PomManager();
            pomManager.createPomForDependencyCopy(refs, pomPath);

            // copy dependency
            downloadPath = fileServiceImp.getCombinePath(mavenPath, MavenUtils.TEMP_FOLDER);
            copyDependencies(metadataPath, mavenPath, downloadPath, forceUpdateFlag);

            // 解压并复制下载的元数据包到maven目录下
            extractMd(downloadPath, mavenPath);
            ExecutorService executorService = Executors.newFixedThreadPool(2);
            if (downloadMdpkgPaths != null && downloadMdpkgPaths.size() > 0) {
                CopyToPackagesRunnable copyToPackagesRunnable = new CopyToPackagesRunnable();
                copyToPackagesRunnable.setDownloadMdpkgPaths(downloadMdpkgPaths);
                copyToPackagesRunnable.setPackagePath(packagePath);
                executorService.submit(copyToPackagesRunnable::run);
            }

            // 下载jar
            if (forceUpdateFlag && ProcessMode.GENERATION.equals(processMode)) {
                InstallJarRunnable installJarRunnable = new InstallJarRunnable();
                installJarRunnable.setPomPath(pomPath);
                installJarRunnable.setDownloadMdpkgPaths(downloadMdpkgPaths);
                installJarRunnable.setPomDirPath(metadataPath);
                installJarRunnable.setForceUpdateFlag(true);
                installJarRunnable.setMavenPath(mavenPath);
                installJarRunnable.setDownloadPath(downloadPath);
                executorService.submit(installJarRunnable::run);
            }
        } finally {
            if (!forceUpdateFlag || !ProcessMode.GENERATION.equals(processMode)) {
                //删除pom.xml
                if (fileServiceImp.isFileExist(pomPath)) {
                    fileServiceImp.fileDelete(pomPath);
                }
                //删除\projects\maven/temp
                if (fileServiceImp.isDirectoryExist(downloadPath)) {
                    fileServiceImp.deleteAllFilesUnderDirectory(downloadPath);
                }
            }
        }
    }

    public void installMdpkg(String groupId, String artifactId, String version, String absolutePath,
        Boolean forceUpdateFlag, String mavenPath, String packagePath) throws IOException {
        //创建pom文件,对应的mdproj添加依赖信息
        absolutePath = metadataProjectCoreService.getProjPath(absolutePath);
        String metadataPath = absolutePath + File.separator + "metadata";
        String desPath = new File(mavenPath).getAbsolutePath();
        if (!fileServiceImp.isDirectoryExist(desPath)) {
            fileServiceImp.createDirectory(desPath);
        }
        try {
            boolean isDone = createPomAndDownloadMdproj(groupId, artifactId, version, metadataPath, forceUpdateFlag, mavenPath);
            if (!isDone) {
                throw new RuntimeException("获取元数据依赖失败");
            }
            if (!addDependencyToMdproj(groupId, artifactId, version, metadataPath)) {
                throw new RuntimeException("添加依赖包到mdproj失败");
            }

            //解压并复制下载的元数据包到/projects/maven目录下，结构和nuget对齐。
            extractMd(downloadPath, mavenPath);
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            if (downloadMdpkgPaths != null && downloadMdpkgPaths.size() > 0) {
                CopyToPackagesRunnable copyToPackagesRunnable = new CopyToPackagesRunnable();
                copyToPackagesRunnable.setDownloadMdpkgPaths(downloadMdpkgPaths);
                copyToPackagesRunnable.setPackagePath(packagePath);
                executorService.submit(copyToPackagesRunnable::run);
            }

            //下载jar包
            ProcessMode processMode = metadataProjectCoreService.getProcessMode(absolutePath);
            if (version.contains("SNAPSHOT") && forceUpdateFlag && ProcessMode.GENERATION.equals(processMode)) {
                InstallSingleJarRunnable installSingleJarRunnable = new InstallSingleJarRunnable();
                installSingleJarRunnable.setPomDirPath(metadataPath);
                installSingleJarRunnable.setDownloadPath(downloadPath);
                installSingleJarRunnable.setMavenPath(mavenPath);
                executorService.submit(installSingleJarRunnable::run);
            }

            //添加api依赖
            String apiPath = fileServiceImp.getApiModulePath(absolutePath);
            if (apiPath != null && !apiPath.isEmpty()) {
                String pomApi = fileServiceImp.getCombinePath(fileServiceImp.getApiModulePath(absolutePath), MavenUtils.POM_FILE);
                if (fileServiceImp.isFileExist(pomApi)) {
                    addProjectDependency(pomApi, groupId, artifactId, version);
                }
            }

        } finally {
            String metadataPomPath = fileServiceImp.getCombinePath(metadataPath, MavenUtils.POM_FILE);
            //删除pom.xml
            if (fileServiceImp.isFileExist(metadataPomPath)) {
                fileServiceImp.fileDelete(metadataPomPath);
            }
            //删除\projects\maven/temp
            if (!version.contains("SNAPSHOT") || !forceUpdateFlag) {
                if (fileServiceImp.isDirectoryExist(downloadPath)) {
                    fileServiceImp.deleteAllFilesUnderDirectory(downloadPath);
                }
            }
        }
    }

    public void uninstallMdpkg(String absolutePath, String packName, String mavenPath) {
        int divideIndex = packName.indexOf("-");
        String groupId = packName.substring(0, divideIndex);
        String artifactId = packName.substring(divideIndex + 1);
        MavenPackageRefs mavenPackageRefs = new MavenPackageRefs();
        mavenPackageRefs.setGroupId(groupId);
        mavenPackageRefs.setArtifactId(artifactId);

        MetadataProject metadataProject = metadataProjectCoreService.getMetadataProjInfo(absolutePath);
        RepositoryFactory.getInstance().getMetadataProjectRepository().removeMavenRefs(absolutePath, metadataProject, mavenPackageRefs);

        PackageNameManager packageNameManager = new PackageNameManager();
        String mdpkgName = packageNameManager.getMdpkgNameByGA(groupId, artifactId, mavenPath, absolutePath);
        if (mdpkgName.isEmpty()) {
            throw new RuntimeException("无法根据" + packName + "获取准确的元数据包名，请手动删除工程引用文件中MetadataPackageRefs节点和ProjectRefs节点中的相关内容。");
        }

        List<String> mdpkgNames = new ArrayList<>();
        mdpkgNames.add(mdpkgName);
        RepositoryFactory.getInstance().getMetadataProjectRepository().removeProjectRefs(absolutePath, metadataProject, mdpkgNames);

        MetadataPackageHeader metadataPackageHeader = new MetadataPackageHeader();
        metadataPackageHeader.setName(mdpkgName);
        RepositoryFactory.getInstance().getMetadataProjectRepository().removeRefs(absolutePath, metadataProject, metadataPackageHeader);
    }

    public void addProjectDependency(String pomPath, String groupId, String artifactId, String version) {
        try (FileInputStream fis = new FileInputStream(pomPath)) {
            MavenXpp3Reader reader = new MavenXpp3Reader();
            Model model = reader.read(fis);
            List<Dependency> list = model.getDependencies();
            Dependency dependency = new Dependency();
            dependency.setGroupId(groupId);
            dependency.setArtifactId(artifactId);
            if (version.startsWith("m")) {
                dependency.setVersion(version.substring(1));
            } else {
                dependency.setVersion(version);
            }
            //需要判断依赖信息。依赖信息中不允许有重复。
            boolean isDependencyExits = false;
            for (Dependency dep : list) {
                if (dep.getGroupId().equals(groupId) && dep.getArtifactId().equals(artifactId)) {
                    //如果groupId，artifactId相同，则更新版本号。
                    isDependencyExits = true;
                    if (version.startsWith("m")) {
                        dep.setVersion(version.substring(1));
                    } else {
                        dep.setVersion(version);
                    }
                }
            }
            if (!isDependencyExits) {
                list.add(dependency);
            }
            model.setDependencies(list);
            MavenXpp3Writer mavenXpp3Writer = new MavenXpp3Writer();
            try (FileOutputStream fileOutputStream = new FileOutputStream(pomPath)) {
                mavenXpp3Writer.write(fileOutputStream, model);
            }
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    public void deployReleaseMdVersion(String projPath, String version, String repoId, boolean revert,
        String dependenciesVersion, String mavenPath) throws IOException {
        if (projPath == null || projPath.isEmpty()) {
            throw new IllegalArgumentException("工程路径不能为空");
        }
        if (version == null || version.isEmpty()) {
            throw new IllegalArgumentException("version不能为空");
        }
        String codepath = projPath + File.separator + "java" + File.separator + Utils.getMavenProName();

        String apiPomPath = fileServiceImp.getApiModulePath(projPath) + File.separator + MavenUtils.POM_FILE;
        String revertCommand = "versions:revert";
        String commitCommand = "versions:commit";
        String command = String.format("versions:set -DnewVersion=%s", version);
        //更新release版本号
        mavenUtilsCore.exeMavenCommand(codepath, mavenPath, command);
        //根据前端dependencies进行版本号变更
        if (!StringUtils.isEmpty(dependenciesVersion)) {
            List<Dependency> list = null;
            Model model = null;
            try (FileInputStream fis = new FileInputStream(apiPomPath)) {
                MavenXpp3Reader reader = new MavenXpp3Reader();
                model = reader.read(fis);
                list = model.getDependencies();
                for (Dependency dependency : list) {
                    if (dependency.getVersion() != null) {
                        if (dependency.getVersion().endsWith(MavenUtils.SNAP_SHOT)) {
                            dependency.setVersion(dependenciesVersion);
                        }
                    }
                }

            } catch (IOException | XmlPullParserException e) {
                e.printStackTrace();
            }
            if (model != null) {
                model.setDependencies(list);
            } else {
                throw new RuntimeException("api模块pom.xml内容为空");
            }
            FileOutputStream fos = new FileOutputStream(apiPomPath);
            MavenXpp3Writer mavenXpp3Writer = new MavenXpp3Writer();
            mavenXpp3Writer.write(fos, model);
            fos.close();
            //然后进行推送操作
            try {
                deployMdpkg(projPath, repoId, dependenciesVersion, mavenPath);
            } finally {
                if (revert) {
                    mavenUtilsCore.exeMavenCommand(codepath, mavenPath, revertCommand);
                } else {
                    mavenUtilsCore.exeMavenCommand(codepath, mavenPath, commitCommand);
                }
            }
        }
    }

    public void deploySnapshotMdVersion(String projPath, String repoId, String version, boolean revert,
        String mavenPath) {
        if (projPath == null || projPath.isEmpty()) {
            throw new IllegalArgumentException("工程路径不能为空");
        }
        if (version == null || version.isEmpty()) {
            throw new IllegalArgumentException("version不能为空");
        }

        String codePath = projPath + File.separator + "java" + File.separator + Utils.getMavenProName();
        String revertCommand = "versions:revert";
        String commitCommand = "versions:commit";
        String command = String.format("versions:set -DnewVersion=%s", version);
        //更新版本号
        mavenUtilsCore.exeMavenCommand(codePath, mavenPath, command);
        //然后进行推送操作
        try {
            deployMdpkg(projPath, repoId, null, mavenPath);
        } finally {
            if (revert) {
                mavenUtilsCore.exeMavenCommand(codePath, mavenPath, revertCommand);
            } else {
                mavenUtilsCore.exeMavenCommand(codePath, mavenPath, commitCommand);
            }
        }
    }

    public void deployMdpkgForInterpretation(String projPath, String repoId, String version, String dependencyVersion,
        String mavenPath) {
        final MavenDeploymentForMdpkgManager mavenDeploymentForMdpkgManager = new MavenDeploymentForMdpkgManager();
        mavenDeploymentForMdpkgManager.deploy(projPath, repoId, version, dependencyVersion, mavenPath);
    }

    public void deployMdpkg(String proPath, String repoId, String dependencyVersion, String mavenPath) {
        String repoUrl = getRepoUrl(repoId, mavenPath);
        if (repoUrl == null || repoUrl.isEmpty()) {
            throw new IllegalArgumentException("获取repoUrl失败，请检查" + mavenPath + "/config/settings.xml配置");
        }
        boolean isModelExist = isJavaProjExist(proPath);
        //接下来拷贝元数据
        String moduleApiPath;
        String codePath = proPath + File.separator + "java" + File.separator + Utils.getMavenProName();
        String codePom = fileServiceImp.getCombinePath(codePath, MavenUtils.POM_FILE);
        String apiPomPath;
        String metadataBin = File.separator + "metadata" + File.separator + "bin";
        String metadataBinPath = proPath + metadataBin;
        String metadataPath = File.separator + "metadata";
        String metadataDir = proPath + metadataPath;
        String metadataTempDir = fileServiceImp.getCombinePath(metadataDir, MavenUtils.TEMP_FOLDER);
        if (isModelExist) {
            moduleApiPath = fileServiceImp.getApiModulePath(proPath);
            apiPomPath = fileServiceImp.getCombinePath(moduleApiPath, MavenUtils.POM_FILE);
            //读mdproj获取依赖信息
            metadataProject = metadataProjectCoreService.getMetadataProjInfo(metadataDir);
            List<MavenPackageRefs> refsList = metadataProject.getMavenPackageRefs();
            try {
                //获取api module的groupId,artifactId,version
                apiModuleInfo = getApiModuleInfo(apiPomPath);
                if (apiModuleInfo != null) {
                    boolean isSnapshot = apiModuleInfo.getVersion().endsWith(MavenUtils.SNAP_SHOT);
                    setRepoUrl(codePom, repoUrl, repoId, isSnapshot);
                }
                createPomForDeploy(apiModuleInfo, refsList, metadataTempDir, repoUrl, repoId, dependencyVersion);
                boolean isCreated = createSrcDir(metadataTempDir);
                if (!isCreated) {
                    log.error("创建资源目录失败");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //获取工程下元数据包名
            String metadaPackageName = "";
            List<File> list = fileServiceImp.getAllFiles(metadataBinPath);
            for (File file : list) {
                if (file.getName().endsWith(Utils.getMetadataPackageExtension())) {
                    metadaPackageName = file.getName();
                }
            }
            if (metadaPackageName.isEmpty()) {
                throw new RuntimeException("找不到元数据包,请先编译！");
            }
            String metadataFullPath = metadataBinPath + File.separator + metadaPackageName;
            String destPath = metadataTempDir + File.separator + MavenUtils.RESOURCES_DIR + File.separator + metadaPackageName;
            try {
                fileServiceImp.fileCopy(metadataFullPath, destPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (!mavenUtilsCore.exeMavenCommand(metadataTempDir, mavenPath, "deploy")) {
                    throw new RuntimeException("部署元数据包失败");
                }
            } catch (Exception e) {
                throw new RuntimeException("部署元数据失败", e);
            } finally {
                try {
                    fileServiceImp.deleteAllFilesUnderDirectory(metadataTempDir);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            boolean isApiPushed = mavenUtilsCore.exeMavenCommand(codePath, mavenPath, "deploy -pl api -am");
            if (!isApiPushed) {
                throw new RuntimeException("部署api失败");
            }

            try {
                String processMode = metadataProject.getMetadataPackageInfo().getProcessMode() == null ? "generation" : metadataProject.getMetadataPackageInfo().getProcessMode().toString();
                updateMetadataIndexWithProcessMode(metadaPackageName, metadataBinPath, repoUrl, apiModuleInfo, processMode);
            } catch (Exception e) {
                updateMetadataIndex(metadaPackageName, metadataBinPath, repoUrl, apiModuleInfo);
            }
        } else {
            throw new RuntimeException("未找到Java模板工程目录，请先生成Java代码");
        }
    }

    public MavenComponents getVersionsOfSinglePackage(String sourceUrl, String packName) {
        String[] urlSections = sourceUrl.split("/");
        String repository = urlSections[urlSections.length - 1];
        String groupId = packName.substring(0, packName.indexOf("-"));
        String artifactId = packName.substring(packName.indexOf("-") + 1);
        String url = "https://repos.iec.io/service/rest/v1/search?repository=" +
            repository +
            "&group=" +
            groupId +
            "&name=" +
            artifactId +
            "&sort=version";
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBasicAuth("igix-web-ide", "imUIz2J#mEgmkvRS");
            HttpEntity<Object> entity = new HttpEntity<>(null, headers);
            ResponseEntity<MavenComponents> response = restTemplate.exchange(url, HttpMethod.GET, entity, MavenComponents.class);
            return response.getBody();
        } catch (Exception e) {
            return null;
        }
    }

    public String getMavenPackageLatestVersion(String groupId, String artifactId) {
        // 首先从gsp-releases获取，如果可以连接外网，则可以获取。
        String version = getMavenPackageVersionsFromGsp(groupId, artifactId);
        if ("0.1.0".equals(version)) {
            // 读取settings.xml中的仓库，循环查找版本
            try {
                version = getMavenPackageVersionsFromRepository(groupId, artifactId);
            } catch (MalformedURLException e) {
            }
        }
        return version;
    }

    private String getMavenPackageVersionsFromGsp(String groupId, String artifactId) {
        String version = "0.1.0";
        try {
            String url = "https://repos.iec.io/service/rest/v1/search?repository=maven-gsp-releases&group=" + groupId + "&name=" + artifactId + "&sort=version";
            HttpHeaders headers = new HttpHeaders();
            headers.setBasicAuth("igix-web-ide", "imUIz2J#mEgmkvRS");
            HttpEntity<Object> entity = new HttpEntity<>(null, headers);
            ResponseEntity<MavenComponents> response = restTemplate.exchange(url, HttpMethod.GET, entity, MavenComponents.class);
            // 如果查找到版本，排序获取最新版本
            VersionManager versionManager = new VersionManager();
            for (MavenComponent item : response.getBody().getItems()) {
                version = versionManager.versionCompare(version, item.getVersion().replace("m", ""));
            }
        } catch (Exception e) {
        }
        return version;
    }

    private String getMavenPackageVersionsFromRepository(String groupId,
        String artifactId) throws MalformedURLException {
        String version = "0.1.0";
        final MavenSettings mavenSettings = SpringBeanUtils.getBean(MavenService.class).getMavenSettings();
        if (mavenSettings == null) {
            return version;
        }

        for (MavenSettingsMirror mirror : mavenSettings.getMirrors()) {
            if (mirror.getUrl() == null || mirror.getId() == null) {
                break;
            }

            HttpHeaders headers = null;
            for (MavenSettingsServer server : mavenSettings.getServers()) {
                if (server.getId().equals(mirror.getId())) {
                    headers = new HttpHeaders();
                    headers.setBasicAuth(server.getUsername(), server.getPassword());
                    break;
                }
            }
            if (headers == null) {
                break;
            }

            final URL url = new URL(mirror.getUrl());
            final String[] splitUrl = mirror.getUrl().split("/");
            String repository = splitUrl[splitUrl.length - 1];
            String searchUrl = url.getProtocol() + "://" + url.getHost();
            searchUrl += url.getPort() == -1 ? "" : ":" + url.getPort();
            searchUrl += "/service/rest/v1/search?repository=" + repository + "&group=" + groupId + "&name=" + artifactId + "&sort=version";

            try {
                HttpEntity<Object> entity = new HttpEntity<>(null, headers);
                ResponseEntity<MavenComponents> response = restTemplate.exchange(searchUrl, HttpMethod.GET, entity, MavenComponents.class);
                // 如果查找到版本，排序获取最新版本
                if (response.getBody() != null && response.getBody().getItems() != null && response.getBody().getItems().size() > 0) {
                    for (MavenComponent item : response.getBody().getItems()) {
                        version = versionCompareRelease(version, item.getVersion().replace("m", ""));
                    }
                }
            } catch (Exception e) {
            }
        }
        return version;
    }

    private String versionCompareRelease(String existVersion, String newVersion) {
        if (newVersion.contains("-")) {
            return existVersion;
        } else {
            VersionManager versionManager = new VersionManager();
            return versionManager.versionCompare(existVersion, newVersion);
        }
    }

    public MavenPackageRefs getGAByMdpkgName(String mdpkgName) {
        MavenPackageRefs mavenPackageRefs = new MavenPackageRefs();
        String groupId;
        String artifactId;

        String[] info = mdpkgName.split("\\.");
        switch (info.length) {
            case 2:
                groupId = "com." + info[0].toLowerCase();
                artifactId = String.format("%s-api", info[0]).toLowerCase();
                break;
            case 3:
                groupId = "com." + info[0].toLowerCase();
                artifactId = String.format("%s-api", info[1]).toLowerCase();
                break;
            default:
                groupId = String.format("com.%s.%s", info[0], info[1]).toLowerCase();
                if ("Inspur.Gsp.Common.CommonCmp.mdpkg".equals(mdpkgName)) {
                    artifactId = "gsp-common-commoncomponent-api";
                } else if ("Inspur.Gsp.Common.CommonUdt.mdpkg".equals(mdpkgName)) {
                    artifactId = "gsp-pfcommon-commonudt-api";
                } else {
                    if (info.length > 4) {
                        artifactId = String.format("%s-%s-%s-api", info[2], info[3], info[4]).toLowerCase();
                    } else {
                        artifactId = String.format("%s-%s-%s-api", info[1], info[2], info[3]).toLowerCase();
                    }
                }
        }
        mavenPackageRefs.setGroupId(groupId);
        mavenPackageRefs.setArtifactId(artifactId);
        return mavenPackageRefs;
    }

    public MetadataRepo getRepoList() {
        final MavenSettings mavenSettings = SpringBeanUtils.getBean(MavenService.class).getMavenSettings();
        final List<MavenSettingsMirror> mirrors = mavenSettings.getMirrors();
        final MetadataRepo metadataRepo = transferMirrorsToMetadataRepo(mirrors);
        return metadataRepo;
    }

    public MetadataRepo getHostedRepoList() {
        final List<MavenSettingsMirror> mirrors = getHostedMirrors();
        final MetadataRepo metadataRepo = transferMirrorsToMetadataRepo(mirrors);
        return metadataRepo;
    }

    private List<MavenSettingsMirror> getHostedMirrors() {
        final MavenSettings mavenSettings = SpringBeanUtils.getBean(MavenService.class).getMavenSettings();
        final List<MavenSettingsMirror> mirrorsInSettings = mavenSettings.getMirrors();
        // 去掉aliyun
        List<MavenSettingsMirror> mirrors = mirrorsInSettings.stream().filter(mirror -> !mirror.getUrl().contains(alimavenFlag)).collect(Collectors.toList());
        // 加上gsp和bf
        publicMirrors.forEach(publicMirror -> {
            if (mirrors.stream().noneMatch(mirror -> mirror.getUrl().equals(publicMirror.getUrl()))) {
                mirrors.add(publicMirror);
            }
        });
        return mirrors;
    }

    private MetadataRepo transferMirrorsToMetadataRepo(List<MavenSettingsMirror> mirrors) {
        MetadataRepo metadataRepo = new MetadataRepo();
        List<PackageSourceDto> sourceDtos = new ArrayList<>();
        mirrors.forEach(mirror -> {
            final PackageSourceDto packageSourceDto = new PackageSourceDto();
            packageSourceDto.setId(mirror.getId());
            packageSourceDto.setName(mirror.getName());
            packageSourceDto.setSourceUrl(mirror.getUrl());
            sourceDtos.add(packageSourceDto);
        });
        metadataRepo.setRepoList(sourceDtos);
        return metadataRepo;
    }

    private String handleCompileGoal(String modules) {
        StringBuilder sb;
        if ("all".equals(modules)) {
            sb = new StringBuilder("clean install").append(" -T 100C -Dmaven.test.skip=true");
        } else {
            sb = new StringBuilder("clean install -pl ").append(modules).append(" -T 100C -Dmaven.test.skip=true");
        }

        if (modules.contains("api")) {
            sb.append(" -am ");
        }
        return sb.toString();
    }

    public boolean getInstallFlag(MavenPackageRefs ref, Boolean force, String mavenPath, String packagePath) {
        boolean flagValidMavenRef = ref.getGroupId() != null && ref.getArtifactId() != null && ref.getVersion() != null;
        if (!flagValidMavenRef) {
            String log = "[LCM Warning]" + "存在不合法的maven引用，GAV：" + ref.getGroupId() + "," + ref.getArtifactId() + "," + ref.getVersion();
            LoggerDisruptorQueue.publishEvent(log, CAFContext.current.getUserId());
            return false;
        }

        // 是临时包则下载;maven文件夹下需要元数据包和pom文件，缺一则下载;packages文件夹下需要元数据包，缺少则下载
        boolean installFlag = ref.getVersion().contains(MavenUtils.SNAP_SHOT) && force;
        if (!installFlag) {
            List<File> fileInMaven = fileServiceImp.getAllFiles(mavenPath + File.separator + ref.getGroupId() + "-" + ref.getArtifactId() + "-" + ref.getVersion());
            installFlag = fileInMaven.size() < 2;
            if (!installFlag) {
                File mdpkg = fileInMaven.stream().filter(file -> file.getName().endsWith(".mdpkg")).findAny().orElse(null);
                installFlag = mdpkg == null || !fileServiceImp.isFileExist(packagePath + File.separator + mdpkg.getName() + File.separator + mdpkg.getName());
            }
        }
        return installFlag;
    }

    private void removeProjectRefs(String absolutePath, List<MavenPackageRefs> mavenPackageRefsList,
        List<ProjectHeader> projectHeaderList) {
        if (Utils.isNullOrEmpty(projectHeaderList) || Utils.isNullOrEmpty(mavenPackageRefsList)) {
            return;
        }

        List<MavenPackageRefs> mavenPackageInfoInBo = new ArrayList<>();
        for (ProjectHeader projectHeader : projectHeaderList) {
            String projPath = fileServiceImp.getCombinePath(absolutePath, projectHeader.getProjectPath());
            if (fileServiceImp.isDirectoryExist(projPath)) {
                GspProject gspProjectInfo = new GspProjectCoreService().getGspProjectInfo(projPath);
                MavenPackageRefs mavenPackageRefs = new MavenPackageRefs();
                String[] strings = gspProjectInfo.getProjectNameSpace().split("\\.");
                if (strings.length == 1) {
                    mavenPackageRefs.setGroupId("com." + strings[0].toLowerCase());
                } else {
                    mavenPackageRefs.setGroupId("com." + (strings[0] + "." + strings[1]).toLowerCase());
                }
                mavenPackageRefs.setArtifactId((gspProjectInfo.getAppCode() + "-" + gspProjectInfo.getServiceUnitCode() + "-" + gspProjectInfo.getMetadataProjectName().replace("bo-", "") + "-api").toLowerCase());
                mavenPackageInfoInBo.add(mavenPackageRefs);
            }
        }

        List<MavenPackageRefs> refForExclude = new ArrayList<>();
        if (mavenPackageInfoInBo.size() > 0) {
            for (MavenPackageRefs mavenPackageRefs : mavenPackageRefsList) {
                for (MavenPackageRefs mavenPackageRefsInBo : mavenPackageInfoInBo) {
                    if (mavenPackageRefs.getGroupId().equals(mavenPackageRefsInBo.getGroupId())
                        && mavenPackageRefs.getArtifactId().equals(mavenPackageRefsInBo.getArtifactId())) {
                        refForExclude.add(mavenPackageRefs);
                    }
                }
            }
            for (MavenPackageRefs mavenPackageRefs : refForExclude) {
                mavenPackageRefsList.remove(mavenPackageRefs);
            }
        }
    }

    private void updateMetadataIndex(String mdpkgName, String mdpkgPath, String repoUrl, ApiModuleInfo apiModuleInfo) {
        MetadataPackage metadataPackage = new MetadataCoreManager().getMetadataPackageInfo(mdpkgName, mdpkgPath);
        if (metadataPackage.getMetadataList() != null && metadataPackage.getMetadataList().size() > 0) {
            PackageWithMetadataInfo packageWithMetadataInfo = new PackageWithMetadataInfo();
            packageWithMetadataInfo.setMavenPackageGroupId(apiModuleInfo.getGroupId());
            packageWithMetadataInfo.setMavenPackageArtifactId(apiModuleInfo.getArtifactId());
            packageWithMetadataInfo.setMavenPackageVersion(apiModuleInfo.getVersion());
            packageWithMetadataInfo.setMavenPackageUrl(repoUrl);

            List<MetadataInfoInMaven> metadatas = new ArrayList<>();
            for (GspMetadata metadata : metadataPackage.getMetadataList()) {
                MetadataInfoInMaven tempMetadata = new MetadataInfoInMaven();
                tempMetadata.setMetadataId(metadata.getHeader().getId());
                tempMetadata.setMetadataCode(metadata.getHeader().getCode());
                tempMetadata.setMetadataName(metadata.getHeader().getName());
                tempMetadata.setMetadataNamespace(metadata.getHeader().getNameSpace());
                tempMetadata.setMetadataType(metadata.getHeader().getType());
                tempMetadata.setMetadataIsTranslating(metadata.getHeader().getTranslating() ? "t" : "f");
                tempMetadata.setMdBizobjectId(metadata.getHeader().getBizObjectId());
                tempMetadata.setMetadataPackageCode(metadataPackage.getHeader().getName());
                tempMetadata.setMetadataPackageVersion(metadataPackage.getHeader().getVersion().getVersionString());
                tempMetadata.setMetadataLanguage(metadata.getHeader().getLanguage());
                tempMetadata.setMavenPackageGroupId(apiModuleInfo.getGroupId());
                tempMetadata.setMavenPackageArtifactId(apiModuleInfo.getArtifactId());
                tempMetadata.setMavenPackageVersion(apiModuleInfo.getVersion());
                tempMetadata.setMavenPackageUrl(repoUrl);
                metadatas.add(tempMetadata);
            }

            packageWithMetadataInfo.setMetadatas(metadatas);

            String indexServerUrl = IndexServerManager.getInstance().getIndexServerUrl();
//            IndexServerConfiguration indexServer = IndexServerHelper.getInstance().getIndexServerConfigUration();
//            indexServer.setIP("localhost");
//            String url = "http://" + indexServer.getIP() + ":" + indexServer.getPort() + "/api/v1/metadataindex/mvn";
            restTemplate.postForObject(indexServerUrl, new HttpEntity<>(packageWithMetadataInfo, new HttpHeaders()), boolean.class);
        }
    }

    private ApiModuleInfo getApiModuleInfo(String apiPomPath) {
        try (FileInputStream fis = new FileInputStream(apiPomPath)) {
            MavenXpp3Reader reader = new MavenXpp3Reader();
            Model model = reader.read(fis);
            ApiModuleInfo apiModuleInfo = new ApiModuleInfo();
            //api模块继承父的groupid,artifactid,version,自己没有定义
            if (model.getGroupId() != null) {
                apiModuleInfo.setGroupId(model.getGroupId());
            } else {
                apiModuleInfo.setGroupId(model.getParent().getGroupId());
            }
            if (model.getArtifactId() != null) {
                apiModuleInfo.setArtifactId(model.getArtifactId());
            } else {
                apiModuleInfo.setArtifactId(model.getParent().getArtifactId());
            }
            if (model.getVersion() != null) {
                apiModuleInfo.setVersion(model.getVersion());
            } else {
                apiModuleInfo.setVersion(model.getParent().getVersion());
            }
            return apiModuleInfo;
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setRepoUrl(String codePom, String repoUrl, String repoId, boolean isSnapshot) {
        try (FileInputStream fis = new FileInputStream(codePom)) {
            MavenXpp3Reader reader = new MavenXpp3Reader();
            Model model = reader.read(fis);
            DistributionManagement distributionManagement = getDistributionManagement(repoUrl, repoId, isSnapshot);
            model.setDistributionManagement(distributionManagement);
            MavenXpp3Writer mavenXpp3Writer = new MavenXpp3Writer();
            FileOutputStream fos = new FileOutputStream(codePom);
            mavenXpp3Writer.write(fos, model);
            fos.close();
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    private boolean isJavaProjExist(String proPath) {
        //判断模板工程是否已创建。
        if (fileServiceImp.getProjectPath(proPath) != null) {
            String subpropath = fileServiceImp.getProjectPath(proPath);
            String fullPath = fileServiceImp.getCombinePath(proPath, subpropath);
            List<File> list = fileServiceImp.getAllFiles(fullPath);
            for (File file : list) {
                if (file.toString().endsWith("pom.xml")) {
                    return true;
                }
            }
        }
        return false;
    }

    private String getRepoUrl(String repoId, String mavenPath) {
        String repoUrl = "";
        MetadataRepo metadataRepo = getRepoList();
        if (metadataRepo == null || metadataRepo.getRepoList() == null || metadataRepo.getRepoList().size() <= 0) {
            return null;
        }
        for (PackageSourceDto item : metadataRepo.getRepoList()) {
            if (item.getId().equals(repoId)) {
                repoUrl = item.getSourceUrl();
            }
        }

        return repoUrl;
    }

    private boolean addDependencyToMdproj(String groupid, String artifactid, String version, String mepath) {
        //给对应的mdproj添加依赖信息
        metadataProject = metadataProjectCoreService.getMetadataProjInfo(mepath);
        MavenPackageRefs singlemavenPackageRefs = new MavenPackageRefs();
        singlemavenPackageRefs.setGroupId(groupid);
        singlemavenPackageRefs.setArtifactId(artifactid);
        singlemavenPackageRefs.setVersion(version);
        if (metadataProject.getMavenPackageRefs() == null) {
            List<MavenPackageRefs> list = new ArrayList<>();
            list.add(singlemavenPackageRefs);
            metadataProject.setMavenPackageRefs(list);
        } else {
            //不允许引用多个版本的相同包。
            List<MavenPackageRefs> mavenPackageRefs = metadataProject.getMavenPackageRefs();
            boolean isDependencyExits = false;
            for (MavenPackageRefs dep : mavenPackageRefs) {
                if (dep.getGroupId().equals(groupid) && dep.getArtifactId().equals(artifactid)) {
                    //如果groupId，artifactId相同，则更新版本号。
                    isDependencyExits = true;
                    dep.setVersion(version);
                }
            }
            if (!isDependencyExits) {
                mavenPackageRefs.add(singlemavenPackageRefs);
            }
            metadataProject.setMavenPackageRefs(mavenPackageRefs);
        }
        //实体已更新,序列化到磁盘
        List<File> files = fileServiceImp.getAllFiles(mepath);
        File mdprojFile = null;
        for (File file : files) {
            if (".mdproj".equals(fileServiceImp.getExtension(file))) {
                mdprojFile = file;
            }
        }
        if (mdprojFile == null) {
            throw new RuntimeException(".mdproj文件不存在");
        }
        Utils.writeValue(mdprojFile.getPath(), metadataProject);

        return true;
    }

    private boolean createPomAndDownloadMdproj(String groupid, String artifactid, String version, String path,
        Boolean forceUpdateFlag, String mavenPath) throws IOException {
        File pomFile = new File(fileServiceImp.getCombinePath(path, MavenUtils.POM_FILE));
        if (!pomFile.exists()) {
            if (pomFile.createNewFile()) {
                log.debug("创建下载pom.xml完成");
            }
        }
        FileOutputStream fileOutputStream = new FileOutputStream(pomFile);
        Model model = new Model();
        model.setArtifactId("com.inspur.edp");
        model.setGroupId("download-tools");
        model.setVersion("0.1.0-SNAPSHOT");
        model.setModelVersion("4.0.0");
        model.setPackaging("pom");
        List<Dependency> list = model.getDependencies();
        Dependency dependency = new Dependency();
        dependency.setArtifactId(artifactid);
        dependency.setGroupId(groupid);
        dependency.setVersion(version);
        dependency.setType("zip");
        list.add(dependency);
        model.setDependencies(list);
        MavenXpp3Writer mavenXpp3Writer = new MavenXpp3Writer();
        mavenXpp3Writer.write(fileOutputStream, model);
        fileOutputStream.close();
        downloadPath = fileServiceImp.getCombinePath(mavenPath, MavenUtils.TEMP_FOLDER);

        //获取排除的artifact
        MetadataProject metadataProject = metadataProjectCoreService.getMetadataProjInfo(metadataProjectCoreService.getProjPath(path));
        StringBuilder excludeArtifactIds = new StringBuilder();
        if (metadataProject.getMavenPackageRefs() != null && metadataProject.getMavenPackageRefs().size() > 0) {
            for (MavenPackageRefs mavenPackageRef : metadataProject.getMavenPackageRefs()) {
                if (!mavenPackageRef.getArtifactId().equals(artifactid)) {
                    excludeArtifactIds.append(mavenPackageRef.getArtifactId()).append(",");
                }
            }
        }
        if (excludeArtifactIds.length() > 0) {
            excludeArtifactIds = new StringBuilder(excludeArtifactIds.substring(0, excludeArtifactIds.length() - 1));
        }

        String mvnCommand = String.format(" dependency:copy-dependencies -DoutputDirectory=%s -DincludeTypes=zip -DexcludeArtifactIds=%s -Dmdep.copyPom", downloadPath, excludeArtifactIds.toString());
        mvnCommand = forceUpdateFlag ? mvnCommand + " -U" : mvnCommand;
        //拷贝依赖
        return mavenUtilsCore.exeMavenCommand(path, mavenPath, mvnCommand);
    }

    private void checkModules(String projPath, String modules) {
        String codePath = projPath + File.separator + "java" + File.separator + "code";
        String[] moduleArray = modules.split(",");
        for (String module : moduleArray) {
            checkModule(codePath, module);
        }
//        if (moduleArray.length == 1) {
//            checkModuleTarget(codePath, moduleArray[0]);
//        }
    }

    private void checkModule(String codePath, String module) {
        String srcPath = codePath + File.separator + module + File.separator + "pom.xml";
        if (!fileServiceImp.isFileExist(srcPath)) {
            throw new RuntimeException("未找到" + module + "模块代码pom文件，请检查" + module + "模块代码是否已经成功生成。");
        }
    }

    private MavenPackageRefs handleMavenPackageName(String mdpkgName) {
        MavenPackageRefs mavenPackageRefs = getGAByMdpkgName(mdpkgName);
        String version = getMavenPackageLatestVersion(mavenPackageRefs.getGroupId(), mavenPackageRefs.getArtifactId());
        version = version.startsWith("m") ? version : "m" + version;
        mavenPackageRefs.setVersion(version);
        return mavenPackageRefs;

    }

    private MetadataPackageHeader handleMetadataPackageName(String packName) {
        MetadataPackageHeader packageHeader = new MetadataPackageHeader();
        packageHeader.setName(packName.replace(".mdpkg", ""));
        packageHeader.setVersion(new MetadataPackageVersion("1.0.0"));

        return packageHeader;
    }

    private void restoreSpecificPackage(String path, MavenPackageRefs ref, String mavenPath, String packagePath) {
        if (path == null || path.isEmpty()) {
            throw new IllegalArgumentException("工程路径不能为空");
        }
        if (ref.getGroupId() != null && ref.getArtifactId() != null && ref.getVersion() != null) {
            if (ref.getVersion().contains(MavenUtils.SNAP_SHOT)) {
                try {
                    installMdpkg(ref.getGroupId(), ref.getArtifactId(), ref.getVersion(), path, mavenPath, packagePath);
                } catch (IOException e) {
                    log.error("获取最新元数据包失败，请查看mdproj文件中引用的元数据包是否正确");
                    throw new RuntimeException(e);
                }
            }
            if (fileServiceImp.getAllFiles(mavenPath + "/" + ref.getGroupId() + "-" + ref.getArtifactId() + "-" + ref.getVersion()
            ).size() == 0) {
                log.info("路径" + mavenPath + "/" + ref.getGroupId() + "-" + ref.getArtifactId()
                    + "-" + ref.getVersion() + "元数据不存在，开始下载");
                try {
                    installMdpkg(ref.getGroupId(), ref.getArtifactId(), ref.getVersion(), path, mavenPath, packagePath);
                } catch (IOException e) {
                    log.error("获取最新元数据包失败，请查看mdproj文件中引用的元数据包是否正确");
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private void createPomForDeploy(ApiModuleInfo info, List<MavenPackageRefs> refs, String pomPath, String repoUrl,
        String repoId, String dependencyVersion) throws IOException {
        File pomDir = new File(pomPath);
        if (!pomDir.exists()) {
            if (pomDir.mkdir()) {
                log.debug("创建打包目录完成");
            }
        }
        File pomFile = new File(fileServiceImp.getCombinePath(pomDir.toString(), MavenUtils.POM_FILE));
        if (!pomFile.exists()) {
            if (pomFile.createNewFile()) {
                log.debug("创建打包pom.xml完成");
            }
        }
        try (FileOutputStream fileOutputStream = new FileOutputStream(pomFile)) {
            Model model = new Model();
            //构造父pom信息
            Parent parent = new Parent();
            parent.setArtifactId("metadata-parent");
            parent.setGroupId("com.inspur.edp");
            boolean isSnotshot = apiModuleInfo.getVersion().endsWith(MavenUtils.SNAP_SHOT);
            parent.setVersion("0.1.0");
            model.setParent(parent);
            model.setArtifactId(info.getArtifactId());
            model.setGroupId(info.getGroupId());
            model.setVersion("m" + info.getVersion());
            model.setModelVersion("4.0.0");
            model.setPackaging("metadata-pack");
            DistributionManagement distributionManagement = getDistributionManagement(repoUrl, repoId, isSnotshot);
            model.setDistributionManagement(distributionManagement);
            List<Dependency> list = model.getDependencies();
            if (refs != null && refs.size() > 0) {
                refs.forEach(ref -> {
                    Dependency dependency = new Dependency();
                    dependency.setGroupId(ref.getGroupId());
                    dependency.setArtifactId(ref.getArtifactId());
                    if (dependencyVersion != null && ref.getVersion().endsWith(MavenUtils.SNAP_SHOT)) {

                        dependency.setVersion("m" + dependencyVersion);
                    } else {
                        dependency.setVersion(ref.getVersion());
                    }
                    dependency.setType("zip");
                    list.add(dependency);
                });
            }
            //配置插件，必须配置lcm-metadata-build-maven-plugin

            model.setDependencies(list);
            MavenXpp3Writer mavenXpp3Writer = new MavenXpp3Writer();
            mavenXpp3Writer.write(fileOutputStream, model);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private DistributionManagement getDistributionManagement(String repoUrl, String repoId, boolean isSnotshot) {
        DistributionManagement distributionManagement = new DistributionManagement();
        DeploymentRepository deploymentRepository = new DeploymentRepository();
        deploymentRepository.setId(repoId);
        deploymentRepository.setUrl(repoUrl);
        if (isSnotshot) {
            distributionManagement.setSnapshotRepository(deploymentRepository);
        } else {
            distributionManagement.setRepository(deploymentRepository);
        }
        return distributionManagement;
    }

    private boolean createSrcDir(String mdpkgPath) {
        String resDir = mdpkgPath + File.separator + MavenUtils.RESOURCES_DIR;
        File resourceFile = new File(resDir);
        return resourceFile.mkdirs();
    }

    private void afterCompile(String absolutePath) {
        List<String> types = new ArrayList<>();
        types.add("comp");
        metadataProjectCoreService.setSourceDataModifiedTime(absolutePath, types);
    }

    private void extractMd(String libPath, String mavenPath) {
        lock.lock();
        try {
            // 获取lib下所有的元数据包，解压到/projects/maven下
            List<File> allFiles = fileServiceImp.getAllFiles(libPath);
            downloadMdpkgPaths = new ArrayList<>();
            allFiles.stream().parallel().forEach(file -> {
                String desMetadataDir;
                String exactGroupId = getGroupId(file);
                // 解压元数据包
                if (Utils.getZipSuffix().equals(fileServiceImp.getExtension(file))) {
                    ZipFile zf = null;
                    InputStream in = null;
                    ZipInputStream zin = null;
                    try {
                        zf = new ZipFile(file);
                        in = new BufferedInputStream(new FileInputStream(file));
                        Charset utf8 = StandardCharsets.UTF_8;
                        zin = new ZipInputStream(in, utf8);
                        ZipEntry ze;
                        InputStream is = null;
                        OutputStream outputStream = null;
                        OutputStream packageOutputStream = null;
                        while ((ze = zin.getNextEntry()) != null) {
                            try {
                                if (Utils.getMetadataPackageExtension().equals(fileServiceImp.getExtension(ze.toString()))) {
                                    String fileName = ze.getName();
                                    is = zf.getInputStream(ze);

                                    String fileNameWithoutExt = fileServiceImp.getFileNameWithoutExtension(file);
                                    desMetadataDir = mavenPath + File.separator + exactGroupId + "-" + fileNameWithoutExt;
                                    String outputFilePath = desMetadataDir + File.separator + fileName;
                                    downloadMdpkgPaths.add(outputFilePath);
                                    // 如果是已存在的正式版，则不需要再解压到maven文件夹
                                    if (isReleaseMdpkgExists(desMetadataDir)) {
                                        return;
                                    }

                                    if (!fileServiceImp.isDirectoryExist(desMetadataDir)) {
                                        fileServiceImp.createDirectory(desMetadataDir);
                                    }
                                    String pomFileName = fileNameWithoutExt + Utils.getPomSuffix();
                                    fileServiceImp.fileCopy(libPath + File.separator + pomFileName, desMetadataDir + File.separator + pomFileName);
                                    File outputFile = new File(outputFilePath);
                                    outputStream = new FileOutputStream(outputFile);
                                    int len;
                                    while ((len = is.read()) != -1) {
                                        outputStream.write(len);
                                    }
                                }
                            } catch (IOException e) {
                                log.error(e.toString());
                            } finally {
                                if (is != null) {
                                    is.close();
                                }
                                if (packageOutputStream != null) {
                                    packageOutputStream.close();
                                }
                                if (outputStream != null) {
                                    outputStream.close();
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        assert zin != null;
                        try {
                            zin.closeEntry();
                            zin.close();
                            in.close();
                            zf.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("提取元数据包错误,路径为：" + libPath, e);
        } finally {
            lock.unlock();
        }
    }

    private boolean isReleaseMdpkgExists(String desMetadataDir) {
        return !desMetadataDir.endsWith(Utils.getVersionSuffix()) && fileServiceImp.getAllFiles(desMetadataDir).size() == 2;
    }

    private String getGroupId(File file) {
        PomManager pomManager = new PomManager();
        String groupId = null;
        String pomPath = file.getPath().replace(".zip", ".pom");
        if (fileServiceImp.isFileExist(pomPath)) {
            Model model = pomManager.getModel(pomPath);
            if (model != null) {
                groupId = model.getGroupId();
            }
        }
        return groupId;
    }

    private boolean copyDependencies(String pomDirPath, String mavenPath, String tempPath, boolean forceUpdateFlag) {
        final MavenCommandGenerator mavenCommandGenerator = new MavenCommandGenerator();
        mavenCommandGenerator.setOutputDirectory(tempPath);
        mavenCommandGenerator.setIncludeTypes(PackageType.zip.toString());
        mavenCommandGenerator.setCopyPom(true);
        mavenCommandGenerator.setForceUpdate(forceUpdateFlag);
        final String mavenCommand = mavenCommandGenerator.generateCopydependencies();
        return mavenUtilsCore.exeMavenCommand(pomDirPath, mavenPath, mavenCommand);
    }

    private void updateMetadataIndexWithProcessMode(String mdpkgName, String mdpkgDir, String repoUrl,
        ApiModuleInfo apiModuleInfo, String processMode) {
        final MavenDeploymentForMdpkgManager mavenDeploymentForMdpkgManager = new MavenDeploymentForMdpkgManager();
        Model sourceModel = new Model();
        sourceModel.setGroupId(apiModuleInfo.getGroupId());
        sourceModel.setArtifactId(apiModuleInfo.getArtifactId());
        sourceModel.setVersion(apiModuleInfo.getVersion());
        mavenDeploymentForMdpkgManager.updateMetadataIndex(mdpkgName, mdpkgDir, repoUrl, sourceModel, processMode);
    }
}
