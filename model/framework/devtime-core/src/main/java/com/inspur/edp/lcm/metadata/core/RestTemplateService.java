/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core;

import com.inspur.edp.lcm.metadata.api.mvnEntity.MetadataInfoFilter;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MetadataInfoWithVersion;
import com.inspur.edp.lcm.metadata.api.mvnEntity.PageMetadataInfo;
import com.inspur.edp.lcm.metadata.api.mvnEntity.withprocessmode.MetadataInfoFilterWithProcessMode;
import com.inspur.edp.lcm.metadata.api.mvnEntity.withprocessmode.PageMetadataInfoWithProcessMode;
import java.net.URI;
import java.util.List;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Classname RestTemplateService Description RestTemplate工具类 Date 2019/11/29 17:43
 *
 * @author zhongchq
 * @version 1.0
 */
public class RestTemplateService {

    public static RestTemplateService instance = new RestTemplateService();
    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * post请求
     */
    public PageMetadataInfo getObjectByPost(String remoteIndexUrl, MetadataInfoFilter filter) {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<MetadataInfoFilter> entity = new HttpEntity<>(filter, headers);
        return restTemplate.postForObject(remoteIndexUrl, entity, PageMetadataInfo.class);
    }

    /**
     * post请求
     */
    public PageMetadataInfoWithProcessMode getObjectByPost(String remoteIndexUrl,
        MetadataInfoFilterWithProcessMode filter) {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<MetadataInfoFilterWithProcessMode> entity = new HttpEntity<>(filter, headers);
        return restTemplate.postForObject(remoteIndexUrl, entity, PageMetadataInfoWithProcessMode.class);
    }

    /**
     * get请求
     */
    public List<MetadataInfoWithVersion> getObjectByGet(String url) {
        ParameterizedTypeReference<List<MetadataInfoWithVersion>> typeRef = new ParameterizedTypeReference<List<MetadataInfoWithVersion>>() {
        };
        ResponseEntity<List<MetadataInfoWithVersion>> responseEntity = restTemplate.exchange(new RequestEntity(HttpMethod.GET, URI.create(url)), typeRef);
        if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
            return responseEntity.getBody();
        }
        return null;
    }

    public String getStatusByGet(String url) {
        try {
            final ResponseEntity<Object> responseEntity = restTemplate.getForEntity(url, Object.class);
            return responseEntity.getStatusCode().getReasonPhrase();
        } catch (Exception e) {
            return e.getCause().getMessage();
        }
    }
}

