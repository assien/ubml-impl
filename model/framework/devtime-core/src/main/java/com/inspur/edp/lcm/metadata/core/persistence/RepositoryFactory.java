/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.persistence;

public class RepositoryFactory {
    private RepositoryFactory() {
    }

    ;
    private static RepositoryFactory instance;

    public static RepositoryFactory getInstance() {
        if (instance == null) {
            instance = new RepositoryFactory();
        }
        return instance;
    }

    private MetadataRepository metadataRepository;

    public MetadataRepository getMetadataRepository() {
        if (metadataRepository == null) {
            metadataRepository = new MetadataRepository();
        }
        return metadataRepository;
    }

    private MetadataProjectRepository metadataProjectRepository;

    public MetadataProjectRepository getMetadataProjectRepository() {
        if (metadataProjectRepository == null) {
            metadataProjectRepository = new MetadataProjectRepository();
        }
        return metadataProjectRepository;
    }

    private PomRepository pomRepository;

    public PomRepository getPomRepository() {
        if (pomRepository == null) {
            pomRepository = new PomRepository();
        }
        return pomRepository;
    }

    private MavenSettingsRepository mavenSettingsRepository;

    public MavenSettingsRepository getMavenSettingsRepository() {
        if (mavenSettingsRepository == null) {
            mavenSettingsRepository = new MavenSettingsRepository();
        }
        return mavenSettingsRepository;
    }

}
